#!/usr/bin/env sh

set -eu

installApp() {
  if [ -f "${MURMUR_DATABASE}" ]; then
    chown "${MURMUR_UNAME}:" "${MURMUR_DATABASE}"
  elif [ -d "$(dirname "${MURMUR_DATABASE}")" ]; then
    chown "${MURMUR_UNAME}:" "$(dirname "${MURMUR_DATABASE}")"
  else
    install -dm 0744 -o "${MURMUR_UNAME}" "$(dirname "${MURMUR_DATABASE}")"
  fi

  gomplate \
    --template /etc/templates \
    --file /etc/murmur.goini \
    --out /etc/murmur.ini

  if [ -n "${MURMUR_SUPERUSER_PASSWORD}" ]; then
    logEventStart 'Change superuser password'
    murmur -ini /etc/murmur.ini -supw "${MURMUR_SUPERUSER_PASSWORD}" &
    wait
    logEventEnd 'Change superuser password'
  fi
}

# shellcheck disable=SC1091
main() {
  . /usr/sbin/support/log-events
  . /usr/sbin/support/environment-variables

  logEventStart 'Container init script'

  if [ "$1" = 'murmur' ]; then
    installApp
    logEventStart 'Server'
  fi

  unsetVariables '^MURMUR_.*'
  exec "$@"
}

main "$@"
